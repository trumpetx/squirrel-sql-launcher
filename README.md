# README #

This plugin for Squirrel SQL allows a user to launch SQuirreL with command line options to connect to a specific database.  This is accomplished by creating an alias, connecting to it automatically, and then deleting the alias on exit.

### What's included in launcher.zip? ###

* Plugin jar: launcher.jar
* JDBC jar files available from Maven: MSSQL, PostgreSQL

### Installation and Usage ###

* Unzip to the Squirrel SQL plugins/ folder
* Launch SQuirreL: **java -jar squirrel-sql.jar -url jdbc:postgresql://test-server/db-name -aliasname "My Alias" -username sa -password "$e;<r3T" -driver postgres**