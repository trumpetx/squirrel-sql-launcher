package com.trumeptx.squirrel.plugin;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.*;
import net.sourceforge.squirrel_sql.client.ApplicationArguments;
import net.sourceforge.squirrel_sql.client.DefaultApplicationArgument;
import net.sourceforge.squirrel_sql.client.IApplicationArgument;
import net.sourceforge.squirrel_sql.client.gui.db.SQLAlias;
import net.sourceforge.squirrel_sql.client.plugin.DefaultPlugin;
import net.sourceforge.squirrel_sql.client.plugin.PluginException;
import net.sourceforge.squirrel_sql.fw.id.IIdentifier;
import net.sourceforge.squirrel_sql.fw.id.UidIdentifier;
import net.sourceforge.squirrel_sql.fw.sql.ISQLDriver;
import net.sourceforge.squirrel_sql.fw.util.log.ILogger;
import net.sourceforge.squirrel_sql.fw.util.log.LoggerController;

public class LauncherPlugin extends DefaultPlugin {

    private static ILogger log = LoggerController.createLogger(LauncherPlugin.class);

    private Collection<IApplicationArgument> args = Arrays.asList(new IApplicationArgument[]{
        new DefaultApplicationArgument("userpass", "Combined parameter: User\\Password"),
        new DefaultApplicationArgument("password", "Password to use to connect"),
        new DefaultApplicationArgument("username", "Username to use to connect"),
        new DefaultApplicationArgument("url", "JDBC URL to connect to"),
        new DefaultApplicationArgument("aliasname", "Name of the temporary alias"),
        new DefaultApplicationArgument("driver", "JDBC Driver to use")
    });

    private SQLAlias launchAlias;
    private String aliasname;
    private IIdentifier driverIdentifier;
    private String password;
    private String username;
    private String url;
    private String driver;

    private boolean isNotEmpty(String... strs) {
        for (String s : strs) {
            if (s == null || s.trim().length() == 0) {
                return false;
            }
        }
        return true;
    }

    private void loadJar(String partialName) {
        try {
            File libDir = new File(new File(getPluginJarFilePath()).getParentFile().getAbsolutePath() + File.separator + getInternalName() + File.separator + "lib");
            File file = null;
            for (File f : libDir.listFiles()) {
                if (f.getName().toLowerCase().contains(partialName.toLowerCase())) {
                    file = f;
                    break;
                }
            }

            if (file != null) {
                URL url = file.toURI().toURL();
                URLClassLoader classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
                Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
                method.setAccessible(true);
                method.invoke(classLoader, url);
                log.info("Loaded jar dynamically: " + file.getName());
            } else {
                log.warn(partialName + " jar file not found, unable to load dynamically.");
            }
        } catch (Exception ex) {
            log.error("Error loading " + partialName + " jar file.", ex);
        }
    }

    private String stripQuotes(String str) {
        char[] arr = str.toCharArray();
        if (arr[0] == '"' && arr[arr.length - 1] == '"') {
            return str.substring(1, arr.length - 1);
        }
        return str;
    }

    @Override
    public void initialize() throws PluginException {
        try {
            Iterator<String> argIt = Arrays.asList(ApplicationArguments.getInstance().getRawArguments()).iterator();
            while (argIt.hasNext()) {
                switch (argIt.next()) {
                    case "-aliasname":
                        aliasname = stripQuotes(argIt.next());
                        break;
                    case "-userpass":
                        String userPassStr = stripQuotes(argIt.next());
                        int firstBackslash = userPassStr.indexOf('\\');
                        if (firstBackslash < 0) {
                            username = userPassStr;
                        } else {
                            username = userPassStr.substring(0, firstBackslash);
                            password = userPassStr.substring(firstBackslash + 1, userPassStr.length());
                        }
                        break;
                    case "-password":
                        password = stripQuotes(argIt.next());
                        break;
                    case "-username":
                        username = stripQuotes(argIt.next());
                        break;
                    case "-url":
                        url = stripQuotes(argIt.next());
                        break;
                    case "-driver":
                        driver = stripQuotes(argIt.next());
                        if (isNotEmpty(driver)) {
                            Iterator<ISQLDriver> it = _app.getDataCache().drivers();
                            while (driverIdentifier == null && it.hasNext()) {
                                ISQLDriver d = it.next();
                                if (d.getDriverClassName().toLowerCase().contains(driver.toLowerCase())) {
                                    if (!d.isJDBCDriverClassLoaded()) {
                                        try {
                                            loadJar(driver);
                                            _app.getSQLDriverManager().registerSQLDriver(d);
                                        } catch (Throwable t) {
                                        }
                                    }
                                    if (!d.isJDBCDriverClassLoaded()) {
                                        log.error("Unable to launch database connection - driver is not loaded.");
                                    } else {
                                        driverIdentifier = d.getIdentifier();
                                    }
                                }
                            }
                        }
                        break;

                }
            }
            if (isNotEmpty(url, aliasname) && driverIdentifier != null) {
                log.info("Creating launch alias: name=" + aliasname + ", url=" + url + ", username=" + username + (password == null ? ", password=null" : ", password=*****") + ", driver=" + driver);
                launchAlias = new SQLAlias(new UidIdentifier());
                launchAlias.setName(aliasname);
                launchAlias.setUrl(url);
                launchAlias.setDriverIdentifier(driverIdentifier);
                launchAlias.setConnectAtStartup(true);
                launchAlias.setUserName(username);
                launchAlias.setPassword(password);
                launchAlias.setAutoLogon(isNotEmpty(username, password));
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            _app.getDataCache().addAlias(launchAlias);
                        } catch (Exception e) {
                            log.error("Error adding launch alias", e);
                        }
                    }
                });
            }
        } catch (Exception e) {
            throw new PluginException(e);
        }
    }

    @Override
    public void unload() {
        if (launchAlias != null) {
            _app.getDataCache().removeAlias(launchAlias);
            try {
                _app.getDataCache().saveAliases(new File(applicationFiles.getDatabaseAliasesFile().getAbsolutePath()));
            } catch (Exception e) {
                log.error("Error re-saving SQL aliases on plugin unload", e);
            }
        }
    }

    @Override
    public String getInternalName() {
        return "launcher";
    }

    @Override
    public String getDescriptiveName() {
        return "Launcher Plugin";
    }

    @Override
    public String getAuthor() {
        return "David Greene";
    }

    @Override
    public String getVersion() {
        return "3.7.1";
    }

    @Override
    public Collection<IApplicationArgument> getPluginApplicationArguments() {
        return args;
    }
}
